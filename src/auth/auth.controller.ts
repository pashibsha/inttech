import { Body, Controller, Get, Post } from '@nestjs/common';
import { UserService } from 'src/user/user.service';
import { AuthDto } from 'src/dto/auth.dto';

@Controller('auth')
export class AuthController {
  constructor(
    private readonly userService: UserService,
  ) {}

  @Post('register')
  async register(@Body() registerDto) {
    console.log("register: " + JSON.stringify(registerDto))
    const findUser = await this.userService.findByEmail(registerDto.email)

    if (findUser) {
      throw new Error("The mail is already occupied")
    }

    if (!registerDto.email || !registerDto.password) {
      throw new Error("Not all data has been entered")
    }

    const user = await this.userService.register(registerDto)

    return user
  }

  @Post('login')
  async login(@Body() loginDto: AuthDto) {
    const user = await this.userService.findByEmail(loginDto.email)

    if (!user) {
      throw new Error("User with such email doesn't exist")
    }

    const checkPassword = await this.userService.checkPassword(loginDto)
    if (!checkPassword) {
      throw new Error("Wrong password")
    }

    return user
  }
}
