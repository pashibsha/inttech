import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'

import { AuthDto } from 'src/dto/auth.dto'

import { User as UserEntity } from '../entities/user.entity'

import { User } from 'src/types/user.interface'
import { validateUuid } from 'src/functions/uuid'

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>
  ) {}

  async register(registerDto: AuthDto): Promise<User> {
    const { password } = registerDto

    const user = await this.userRepository.save(this.userRepository.create({
      ...registerDto,
    }))

    return user
  }


  async findById(id: string): Promise<User | null> {
    if (!validateUuid(id)) {
      return null
    }

    const user = await this.userRepository.findOne({
      where: { id },
      select: { id: true, email: true },
    })

    return user ? user : null
  }

  async findByEmail(email: string): Promise<User> {
    const user = await this.userRepository.findOne({
      where: { email },
      select: { id: true, email: true },
    })

    return user
  }

  async checkPassword(loginDto: AuthDto) : Promise<Boolean> {
    const user = await this.userRepository.findOne({
      where: {
        email: loginDto.email,
      },
    })
    
    return user.password.localeCompare(loginDto.password) == 0
  }
}
